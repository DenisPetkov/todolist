package ru.petkov.daotask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.petkov.models.Task;

import java.util.ArrayList;

@Component
public class TaskDAO {

    private Task task;
    private int id;
    private ArrayList<Task> taskList = new ArrayList<>();

    @Autowired
    public TaskDAO(Task task) {
        this.task = task;
    }

    //Временное решение
    //TODO: постоянное добавление.
    public void createNewTask(Task task) {
        task.setId(++id);
        taskList.add(task);
    }

    public Task getOneTask(int id) {
        return taskList.stream().filter(fruit -> fruit.getId() == id).findAny().orElse(null);
    }

    public ArrayList<Task> getAllTask() {
        return taskList;
    }

    //TODO: есть возможность реализовать с лямбда выражением.
    public void deleteTask(int id) {
        taskList.removeIf(p -> p.getId() == id);
    }

    public void update(int id, Task task) {
        Task taskUpdated = getOneTask(id);
        taskUpdated.setDescription(task.getDescription());
    }
}