package ru.petkov.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.petkov.daotask.TaskDAO;
import ru.petkov.models.Task;

@Controller
@RequestMapping("/tasks")
public class TaskController {

    private final TaskDAO taskDAO;

    @Autowired
    public TaskController(TaskDAO taskDAO) {
        this.taskDAO = taskDAO;
    }

    @GetMapping("/tasks")
    public String getAllTasks(Model model) {
        model.addAttribute("task", taskDAO.getAllTask());
        return "/task/tasks";
    }
    @GetMapping("/{id}")
    public String getOneTasks(@PathVariable("id") int id, Model model) {
        model.addAttribute("task",taskDAO.getOneTask(id));
        return "/task/OneTask";
    }

    @GetMapping("/new")
    public String newTask(Model model){
        model.addAttribute("task",new Task());
    return "/task/new";
    }

    @PostMapping()
    public String createTask(@ModelAttribute("task") Task task){
        taskDAO.createNewTask(task);
       return "redirect:/tasks/tasks";
    }

    @DeleteMapping("/{id}")
    public String deleteTask(@PathVariable("id") int id){
        taskDAO.deleteTask(id);
        return "redirect:/tasks/tasks";
    }
    @PatchMapping("/{id}")
    public String update(@ModelAttribute("task")Task task,@PathVariable("id") int id){
        taskDAO.update(id,task);
        return "redirect:/tasks/tasks";
    }
    @GetMapping("/{id}/edit")
    public String edit(Model model,@PathVariable("id") int id){
        model.addAttribute("task",taskDAO.getOneTask(id));
        return "/task/edit";
    }
}
